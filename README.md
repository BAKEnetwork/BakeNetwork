# BAKE Network API

Streamlined delegations of Tez (XTZ) to Tezos bakers who are accepting delegations.

https://bake.network <br>
https://github.com/bakenetwork <br>
https://gitlab.com/BAKEnetwork <br>
https://www.reddit.com/r/bakenetwork <br>
https://www.twitter.com/bakenetwork <br>
https://www.linkedin.com/groups/13702950 <br>



## Getting Started

Trusted bakers who have matriculated into the BAKE Network are granted access keys to authenticate themselves and connect to the BAKE Network API.  To be accepted as a trusted baker, you must apply to enroll. 



#### Step 1 — Becoming a Trusted Baker

Complete and submit this form: https://forms.gle/1ocv3U6teEHvaCHa7 which is (also accessable from https://bake.network)

If you pass the vetting process, you'll be given your own unique access keys.

#### Step 2 — Connect to BAKE Network

Using your access keys and instructions connect to the API and BAKE Network will fill your baker's delegation capacity so that you earn the optimal amount of rewards on each bake. 

#### Step 3 — Use the Dashboard

BAKE Network Dashboard allows you to edit your data, maintain security, monitor delegations, and follow rewards transactions with ease.



## Authors

Kevin Mehrabi [Persuasia](https://github.com/persuasia)

[@kmehrabi](https://www.twitter.com/kmehrabi)

Dray (aka Debajyoti Ray) [debray](https://github.com/orgs/bakenetwork/people/debray)

[@docdrayai](https://www.twitter.com/docdrayai)



## License

This project is licensed under the MIT License - see the [LICENSE.md](https://gist.github.com/BAKEnetwork/LICENSE.md) file for details.
